from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QDesktopWidget,
    QSystemTrayIcon,
    QMenu,
    QAction,
)
from PyQt5.QtGui import (
    QPixmap,
    QIcon,
)

from PyQt5.QtCore import QThread, QTimer, pyqtSignal

from pyqtconfig import ConfigManager
from datetime import datetime
import sys, os
from adcData import set_info_DPN_MCT
from dateutil.tz import gettz


# DATA INICIAL I FINAL PER GENERAR ELS FITXERS DPN I MCT
# 2023-10-29 02:59:00 +02:00
# 2023-10-29 03:00:00 +01:00
data_inicial = datetime(2023, 12, 27, 20, 32, 40, tzinfo=gettz("Spain/Madrid"))
data_final = datetime(2023, 12, 27, 21, 59, 59, tzinfo=gettz("Spain/Madrid"))
minut_inicial = str(0)

global_stats = {
    "CodiEmis": "01087",
    "IDFocus": "GU",
    "IDLegis": "05",
    "IDSubmode": "01",
    "Cabal": "50",
    "N_Mitjanes_LLA": "7200",
    "Acumulat_Llindar": 120,
    "Acumulat_Mitjanes_LLA": 97,
    "252_NSerie": "N1-RO-672",
    "252_FC(c)x2": 0,
    "252_FC(b)x": 1,
    "252_FC(a)": 0,
    "252_Ival": 0,
    "252_IDUnitat": "11",
    "252_IC": "",
    "252_Hores_Substitucio": 10,
    "252_NumMin": 10,
    "252_V_IMP": "",
    "252_F_IMP": "NN",
    "252_V_GEH": "",
    "252_F_GEH": "NN",
    "252_Llindar_Anomal": 5,
    "261_NSerie": "N1-RO-672",
    "261_FC(c)x2": 0,
    "261_FC(b)x": 0.98,
    "261_FC(a)": 3.05,
    "261_Ival": 0,
    "261_IDUnitat": "09",
    "261_IC": 0.1,
    "261_NumMin": 60,
    "261_V_IMP": "",
    "261_F_IMP": "NN",
    "261_V_GEH": "",
    "261_F_GEH": "NN",
    "261_Llindar_Anomal": 100,
    "220_NSerie": "N1-RO-672",
    "220_FC(c)x2": 0,
    "220_FC(b)x": 0.98,
    "220_FC(a)": 3.05,
    "220_Ival": 0,
    "220_IDUnitat": "09",
    "220_IC": 0.1,
    "220_NumMin": 60,
    "220_V_IMP": "",
    "220_F_IMP": "NN",
    "220_V_GEH": "",
    "220_F_GEH": "NN",
    "220_Llindar_Anomal": 100,
}


def resource_path(relative_path):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Logo(QWidget):
    def __init__(self):
        super().__init__()

        # creating label
        self.label = QLabel(self)

        # loading image
        self.pixmap = QPixmap(resource_path("utidlogo.png"))

        # adding image to label
        self.label.setPixmap(self.pixmap)

        # Optional, resize label to image size
        self.label.resize(self.pixmap.width(), self.pixmap.height())


class WorkerThread(QThread):
    def __init__(self, parent):
        QThread.__init__(self, parent)
        self.parent = parent
        self.finished = pyqtSignal()

    def start(self):
        self._timer = QTimer(self)
        self._timer.timeout.connect(self.process)
        self._timer.start(200)

    def stop(self):
        self._timer.stop()
        self.finished.emit()

    def process(self):
        last_datetime_read = self.parent.config.as_dict()["last_datetime_read"]
        if last_datetime_read <= int(data_final.timestamp()):
            set_info_DPN_MCT(self.parent.config, last_datetime_read)
            last_datetime_read += 60
            self.parent.config.set("last_datetime_read", last_datetime_read)
        else:
            self.parent.app.quit()


class MainWindow(QMainWindow):
    def __init__(self, app):
        super(MainWindow, self).__init__()
        self.app = app
        self.config = ConfigManager()
        defaults = {}
        defaults["initMinute"] = minut_inicial
        defaults["initDate"] = data_inicial
        defaults["last_datetime_read"] = int(data_inicial.timestamp())
        defaults["Acumulat_Mitjanes_LLA_actual_year"] = str(2022)
        defaults["Acumulat_Mitjanes_LLA"] = "0"
        defaults["Acumulat_Llindar"] = "0"
        self.config.set_defaults({**defaults, **global_stats})

        layout = QVBoxLayout()
        layout1 = QHBoxLayout()
        layout1.setContentsMargins(0, 0, 0, 0)
        layout1.setSpacing(0)

        layout1.addWidget(Logo())

        layout.addLayout(layout1)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

    def initUI(self):
        self.setWindowTitle("UTID Innovation and Development Technical Unit")
        self.resize(600, 600)
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    window = MainWindow(app)
    icon = QIcon(resource_path("icon.png"))
    tray = QSystemTrayIcon()
    tray.setIcon(icon)
    tray.setVisible(True)
    menu = QMenu()
    option0 = QAction("Obrir")
    option0.triggered.connect(window.show)
    menu.addAction(option0)
    quit = QAction("Tancar")
    quit.triggered.connect(app.quit)
    menu.addAction(quit)
    tray.setContextMenu(menu)
    window.initUI()
    app.setWindowIcon(QIcon(resource_path("icon.png")))

    thread = QThread()
    worker = WorkerThread(window)
    worker.moveToThread(thread)
    thread.started.connect(worker.start)
    thread.start()

    sys.exit(app.exec_())
