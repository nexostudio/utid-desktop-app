import logging
import logging.handlers
import sys
import os

LOG_FILEPATH = "log/app.log"


def setup_logger(log_file=LOG_FILEPATH):
    """Configura el logger para la aplicación con rotación diaria y eliminación de logs antiguos."""
    # Crear un logger
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # Crear un formato para los mensajes de log
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    # Crear un manejador para rotación de archivo diario
    log_dir = os.path.dirname(log_file)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # Rotación diaria y eliminación de archivos mayores a 30 días
    file_handler = logging.handlers.TimedRotatingFileHandler(
        log_file, when="midnight", interval=1, backupCount=30
    )
    file_handler.setFormatter(formatter)

    # Agregar el manejador al logger
    logger.addHandler(file_handler)

    return logger


def exception_hook(exctype, value, tb):
    """Función personalizada para capturar excepciones no manejadas."""
    logger = logging.getLogger()
    logger.error("Excepción no manejada", exc_info=(exctype, value, tb))


# Establecer el hook global para capturar todas las excepciones no manejadas
sys.excepthook = exception_hook
