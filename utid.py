from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QLineEdit,
    QWidget,
    QFormLayout,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QDesktopWidget,
    QComboBox,
    QFrame,
    QSystemTrayIcon,
    QMenu,
    QAction,
    QDialog,
    QDialogButtonBox,
    QPushButton,
    QTextEdit,
)
from PyQt5.QtGui import (
    QIntValidator,
    QDoubleValidator,
    QFont,
    QRegExpValidator,
    QPixmap,
    QIcon,
)

from PyQt5.QtCore import (
    QDate,
    QTimer,
    QTime,
    Qt,
    QRegExp,
    pyqtSlot,
    pyqtSignal,
    QSettings,
    QCoreApplication,
    QProcess,
    QThread,
)

import sys, os
from functools import partial

from pyqtconfig import QSettingsManager, ConfigManager

from adcData import LIST_GAS, set_info_DPN_MCT, get_utc_actual, reboot_system, GAS_DICT

from datetime import datetime
from dateutil import parser
from dateutil.tz import gettz, tzutc
from logger import setup_logger, LOG_FILEPATH

VERSION = "1.15"

DEFAULT_DISABLED_MCT_PERIFERIC_GAS = [
    "109",
    "110",
    "130",
    "252",
    "262",
]


ACTUAL_LIST_GAS = None
FX_PREPARED_SECOND = 43


def windows_registry_set_key(key_path, value):
    from winreg import CreateKey, SetValue, HKEY_CURRENT_USER, REG_SZ

    with CreateKey(HKEY_CURRENT_USER, key_path) as sub_key:
        SetValue(sub_key, None, REG_SZ, value)


def resource_path(relative_path):
    if hasattr(sys, "_MEIPASS"):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class LogWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Finestra de Logs")
        self.setGeometry(150, 150, 600, 400)

        self.layout = QVBoxLayout()

        # Widget para mostrar los logs
        self.log_display = QTextEdit(self)
        self.log_display.setReadOnly(True)

        self.layout.addWidget(self.log_display)

        self.setLayout(self.layout)

        # Timer para actualizar los logs
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_log)
        self.timer.start(10000)  # Actualizar cada segundo

    def update_log(self):
        try:
            with open(LOG_FILEPATH, "r") as log_file:
                log_content = log_file.read()
            self.log_display.setPlainText(log_content)
            self.log_display.moveCursor(self.log_display.textCursor().End)
        except FileNotFoundError:
            self.log_display.setPlainText("No s'ha generat el fitxer de log.")


class GlobalForm(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.layout = QFormLayout()

        self.e0 = QLabel()
        self.e0.setFont(QFont("Arial", 25))
        self.e0.setText("Paràmetres globals")
        self.layout.addRow(self.e0)

        self.e1 = QLineEdit()
        self.e1.setValidator(QIntValidator())
        self.e1.setMaxLength(5)
        self.e1.setAlignment(Qt.AlignRight)
        self.e1.setFont(QFont("Arial", 20))
        parent.config.add_handler("CodiEmis", self.e1)
        self.layout.addRow("CodiEmis", self.e1)

        self.e2 = QLineEdit()
        self.e2.setValidator(QRegExpValidator(QRegExp("^[A-Z0-9]{2}$")))
        self.e2.setMaxLength(2)
        self.e2.setAlignment(Qt.AlignRight)
        self.e2.setFont(QFont("Arial", 20))
        parent.config.add_handler("IDFocus", self.e2)
        self.layout.addRow("IDFocus", self.e2)

        self.e3 = QLineEdit()
        self.e3.setValidator(QRegExpValidator(QRegExp("^[0-9]{2}$")))
        self.e3.setMaxLength(2)
        self.e3.setAlignment(Qt.AlignRight)
        self.e3.setFont(QFont("Arial", 20))
        parent.config.add_handler("IDLegis", self.e3)
        self.layout.addRow("IDLegis", self.e3)

        self.e4 = QLineEdit()
        self.e4.setValidator(QRegExpValidator(QRegExp("^[0-9]{2}$")))
        self.e4.setMaxLength(2)
        self.e4.setAlignment(Qt.AlignRight)
        self.e4.setFont(QFont("Arial", 20))
        parent.config.add_handler("IDSubmode", self.e4)
        self.layout.addRow("IDSubmode", self.e4)

        self.e5 = QLineEdit()
        self.e5.setValidator(QRegExpValidator(QRegExp("^[0-9]{5}$")))
        self.e5.setMaxLength(5)
        self.e5.setAlignment(Qt.AlignRight)
        self.e5.setFont(QFont("Arial", 20))
        parent.config.add_handler("Cabal", self.e5)
        self.layout.addRow("Cabal", self.e5)

        self.e12 = QLineEdit()
        self.e12.setValidator(QDoubleValidator())
        self.e12.setAlignment(Qt.AlignRight)
        self.e12.setFont(QFont("Arial", 20))
        parent.config.add_handler("N_Mitjanes_LLA", self.e12)
        self.layout.addRow("N_Mitjanes_LLA", self.e12)

        self.e13 = QLineEdit()
        self.e13.setReadOnly(True)
        self.e13.setAlignment(Qt.AlignRight)
        self.e13.setFont(QFont("Arial", 20))
        parent.config.add_handler(f"text_Acumulat_Llindar", self.e13)
        self.layout.addRow("Acumulat_Llindar", self.e13)

        self.e14 = QLineEdit()
        self.e14.setReadOnly(True)
        self.e14.setAlignment(Qt.AlignRight)
        self.e14.setFont(QFont("Arial", 20))
        parent.config.add_handler(f"text_Acumulat_Mitjanes_LLA", self.e14)
        self.layout.addRow("Acumulat_Mitjanes_LLA", self.e14)

        self.layout.addWidget(Buttons(parent))
        self.setLayout(self.layout)


class DPNForm(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.layouts = {}
        self.frames = {}
        self.verticalLayout = QVBoxLayout(self)
        for code in LIST_GAS():
            self.frames[code] = QFrame()
            self.layouts[code] = QFormLayout()
            self.frames[code].setLayout(self.layouts[code])
            e0 = QLabel()
            e0.setFont(QFont("Arial", 25))
            e0.setText("Paràmetres DPN")
            self.layouts[code].addRow(e0)

            e1 = QLineEdit()
            # e1.setValidator(QIntValidator()) #string
            # e1.setMaxLength(5)
            e1.setAlignment(Qt.AlignRight)
            e1.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_NSerie", e1)
            self.layouts[code].addRow("NSerie", e1)

            e2 = QLineEdit()
            e2.setValidator(QDoubleValidator())
            e2.setAlignment(Qt.AlignRight)
            e2.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_FC(c)x2", e2)
            self.layouts[code].addRow("FC(c)x2", e2)

            e3 = QLineEdit()
            e3.setValidator(QDoubleValidator())
            e3.setAlignment(Qt.AlignRight)
            e3.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_FC(b)x", e3)
            self.layouts[code].addRow("FC(b)x", e3)

            e4 = QLineEdit()
            e4.setValidator(QDoubleValidator())
            e4.setAlignment(Qt.AlignRight)
            e4.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_FC(a)", e4)
            self.layouts[code].addRow("FC(a)", e4)

            e5 = QLineEdit()
            e5.setValidator(QDoubleValidator())
            e5.setAlignment(Qt.AlignRight)
            e5.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_Ival", e5)
            self.layouts[code].addRow("Ival", e5)

            e7 = QLineEdit()
            e7.setValidator(QRegExpValidator(QRegExp("^[0-9]{2}$")))
            e7.setAlignment(Qt.AlignRight)
            e7.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_IDUnitat", e7)
            self.layouts[code].addRow("IDUnitat", e7)

            e8 = QLineEdit()
            e8.setValidator(QDoubleValidator())
            e8.setAlignment(Qt.AlignRight)
            e8.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_IC", e8)
            self.layouts[code].addRow("IC", e8)

            if code == "252":
                e8 = QLineEdit()
                e8.setValidator(QIntValidator())
                e8.setAlignment(Qt.AlignRight)
                e8.setFont(QFont("Arial", 20))
                parent.config.add_handler("Hores_Substitucio", e8)
                self.layouts[code].addRow("Hores Substitució", e8)

            self.frames[code].hide()
            self.verticalLayout.addWidget(self.frames[code])

    def show(self, code):
        for frame in self.frames.values():
            frame.hide()
        self.frames[code].show()


class MCTForm(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.layouts = {}
        self.frames = {}
        self.alerts = {}
        self.verticalLayout = QVBoxLayout(self)
        for code in LIST_GAS():
            self.frames[code] = QFrame()
            self.layouts[code] = QFormLayout()
            self.frames[code].setLayout(self.layouts[code])
            e0 = QLabel()
            e0.setFont(QFont("Arial", 25))
            e0.setText("Paràmetres MCT")
            self.layouts[code].addRow(e0)

            e1 = QComboBox()
            e1.addItem("10")
            e1.addItem("30")
            e1.addItem("60")
            e1.setFont(QFont("Arial", 15))
            parent.config.add_handler(f"{code}_NumMin", e1)
            self.layouts[code].addRow("NumMin", e1)

            # e21 = QLineEdit()
            # e21.setValidator(QIntValidator())
            # e21.setMaxLength(5)
            # e21.setAlignment(Qt.AlignRight)
            # e21.setFont(QFont("Arial",20))
            # parent.config.add_handler(f'{code}_V_VLE', e21)
            # self.layouts[code].addRow("V_VLE", e21)

            # e2 = QLineEdit()
            # e2.setValidator(QIntValidator())
            # e2.setMaxLength(5)
            # e2.setAlignment(Qt.AlignRight)
            # e2.setFont(QFont("Arial",20))
            # parent.config.add_handler(f'{code}_F_VLE', e2)
            # self.layouts[code].addRow("F_VLE", e2)

            e3 = QLineEdit()
            e3.setValidator(QDoubleValidator())
            e3.setAlignment(Qt.AlignRight)
            e3.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_V_IMP", e3)
            self.layouts[code].addRow("V_IMP", e3)

            e4 = QLineEdit()
            e4.setValidator(QRegExpValidator(QRegExp("^[A-Z]{2}$")))
            e4.setMaxLength(2)
            e4.setAlignment(Qt.AlignRight)
            e4.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_F_IMP", e4)
            self.layouts[code].addRow("F_IMP", e4)

            e9 = QLineEdit()
            e9.setValidator(QDoubleValidator())
            e9.setAlignment(Qt.AlignRight)
            e9.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_V_GEH", e9)
            self.layouts[code].addRow("V_GEH", e9)

            e10 = QLineEdit()
            e10.setValidator(QRegExpValidator(QRegExp("^[A-Z]{2}$")))
            e10.setMaxLength(2)
            e10.setAlignment(Qt.AlignRight)
            e10.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_F_GEH", e10)
            self.layouts[code].addRow("F_GEH", e10)

            e11 = QLineEdit()
            e11.setValidator(QDoubleValidator())
            e11.setAlignment(Qt.AlignRight)
            e11.setFont(QFont("Arial", 20))
            parent.config.add_handler(f"{code}_Llindar_Anomal", e11)
            self.layouts[code].addRow("Llindar_Anòmal", e11)

            hlayout = QHBoxLayout()
            e12 = QLabel()
            e12.setAlignment(Qt.AlignLeft)
            e12.setFont(QFont("Arial", 20))
            e12.setText("Indisponibilitat 10 dies")
            hlayout.addWidget(e12)

            e13 = QLabel()
            e13.setAlignment(Qt.AlignLeft)
            e12.setFont(QFont("Arial", 20))
            e13.setFixedSize(40, 40)
            e13.setStyleSheet("QLabel { background-color : lightgreen; }")
            hlayout.addWidget(e13)
            self.layouts[code].addRow(hlayout)
            self.alerts[code] = e13

            self.frames[code].hide()
            self.verticalLayout.addWidget(self.frames[code])

    def update_indisponibilitat(self, code):
        try:
            indisponibilitat = int(
                self.parent.config.get(f"{code}_Acumulat_Indisponibilitat")
            )
        except:
            self.parent.logger.info(
                f"Error obteniendo {code}_Acumulat_Indisponibilitat. Se añade indisponibilidad 0"
            )
            indisponibilitat = 0
        if indisponibilitat >= 10:
            self.alerts[code].setStyleSheet("QLabel { background-color : red; }")
        else:
            self.alerts[code].setStyleSheet("QLabel { background-color : lightgreen; }")

    def show(self, code):
        for frame in self.frames.values():
            frame.hide()
        self.frames[code].show()
        self.update_indisponibilitat(code)


class Clock(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        layout = QFormLayout()
        font = QFont("Arial", 20, QFont.Bold)

        self.label = QLabel()
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setFont(font)
        layout.addRow(self.label)

        self.label2 = QLabel()
        self.label2.setAlignment(Qt.AlignCenter)
        self.label2.setFont(font)
        now = QDate.currentDate()
        self.label2.setFixedSize(250, 50)
        self.label2.setText(f"Día: {now.toString('dd/MM/yyyy')}")
        layout.addRow(self.label2)

        current_time = QTime.currentTime()
        label_time = current_time.toString("hh:mm:ss")
        self.label.setFixedSize(250, 50)
        self.label.setText(f"Hora: {label_time}")

        self.label.setStyleSheet("border: 1px solid black;")
        self.label2.setStyleSheet("border: 1px solid black;")

        self.setLayout(layout)

        timer = QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)

        timersave = QTimer(self)
        timersave.timeout.connect(self.autoSave)
        timersave.start(1000)

    def showTime(self):
        current_time = QTime.currentTime()
        label_time = current_time.toString("hh:mm:ss")
        self.label.setText(f"Hora: {label_time}")
        now = QDate.currentDate()

        n = datetime.now()
        self.label2.setText(f"Día: {now.toString('dd/MM/yyyy')}")

        force_data_reboot = (
            parser.parse(self.parent.config.get("force_data_reboot"))
            if self.parent.config.get("force_data_reboot")
            else None
        )
        if (
            force_data_reboot
            and (get_utc_actual() - force_data_reboot).total_seconds() >= 3600
        ):
            force_data_reboot_confirm = (
                parser.parse(self.parent.config.get("force_data_reboot_confirm"))
                if self.parent.config.get("force_data_reboot_confirm")
                else None
            )
            if not force_data_reboot_confirm or (
                get_utc_actual() - force_data_reboot_confirm
            ).total_seconds() >= (3600 * 24):
                self.parent.show_popup()

        if not ACTUAL_LIST_GAS and n.second % 10 == 0:
            QCoreApplication.quit()
            QProcess.startDetached(sys.executable, sys.argv)

    def autoSave(self):
        for key, value in self.parent.config.as_dict().items():
            self.parent.qsettings.setValue(key, value)


class WarningPanel(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.verticalLayout = QVBoxLayout(self)
        timersave = QTimer(self)
        timersave.timeout.connect(self.check)
        timersave.start(10000)
        self.layout = QHBoxLayout()
        self.layout2 = QHBoxLayout()
        self.layout3 = QHBoxLayout()
        font = QFont("Arial", 20, QFont.Bold)

        self.label = QLabel()
        self.label.setAlignment(Qt.AlignLeft)
        self.label.setFont(font)
        self.label.setText("Superació valors anòmals")
        self.layout.addWidget(self.label)

        self.label2 = QLabel()
        self.label2.setAlignment(Qt.AlignLeft)
        self.label2.setFont(font)
        self.label2.setFixedSize(40, 40)
        self.label2.setStyleSheet("QLabel { background-color : lightgreen; }")
        self.layout.addWidget(self.label2)

        self.label3 = QLabel()
        self.label3.setAlignment(Qt.AlignLeft)
        self.label3.setFont(font)
        self.label3.setText("Superació 24h")
        self.layout2.addWidget(self.label3)

        self.label4 = QLabel()
        self.label4.setAlignment(Qt.AlignLeft)
        self.label4.setFont(font)
        self.label4.setFixedSize(40, 40)
        self.label4.setStyleSheet("QLabel { background-color : lightgreen; }")
        self.layout2.addWidget(self.label4)

        self.label5 = QLabel()
        self.label5.setAlignment(Qt.AlignLeft)
        self.label5.setFont(font)
        self.label5.setText("Substitució oxigen")
        self.layout3.addWidget(self.label5)

        self.label6 = QLabel()
        self.label6.setAlignment(Qt.AlignLeft)
        self.label6.setFont(font)
        self.label6.setFixedSize(40, 40)
        self.label6.setStyleSheet("QLabel { background-color : lightgreen; }")
        self.layout3.addWidget(self.label6)

        self.verticalLayout.addLayout(self.layout)
        self.verticalLayout.addLayout(self.layout2)
        self.verticalLayout.addLayout(self.layout3)

        self.check()

    def check(self):
        try:
            nmitjanes = int(self.parent.config.get("N_Mitjanes_LLA"))
            acumulat = int(self.parent.config.get("Acumulat_Mitjanes_LLA"))
        except:
            self.parent.logger.info(
                f"Error obteniendo N_Mitjanes_LLA. Se añade N_Mitjanes_LLA 0"
            )
            self.parent.logger.info(
                f"Error obteniendo Acumulat_Mitjanes_LLA. Se añade Acumulat_Mitjanes_LLA 0"
            )
            nmitjanes = 0
            acumulat = 0
        if acumulat >= nmitjanes and acumulat > 0:
            self.label2.setStyleSheet("QLabel { background-color : red; }")
        else:
            self.label2.setStyleSheet("QLabel { background-color : lightgreen; }")

        if acumulat / 60 >= 24:
            self.label4.setStyleSheet("QLabel { background-color : red; }")
        else:
            self.label4.setStyleSheet("QLabel { background-color : lightgreen; }")

        try:
            horessubstitucio = int(self.parent.config.get("Hores_Substitucio"))
            comptador = float(self.parent.config.get("Acumulat_Hores_Substitucio"))
        except:
            self.parent.logger.info(
                f"Error obteniendo Hores_Substitucio. Se añade Hores_Substitucio 0"
            )
            self.parent.logger.info(
                f"Error obteniendo Acumulat_Hores_Substitucio. Se añade Acumulat_Hores_Substitucio -1"
            )
            horessubstitucio = 0
            comptador = -1
        if comptador >= horessubstitucio:
            self.label6.setStyleSheet("QLabel { background-color : red; }")
        else:
            self.label6.setStyleSheet("QLabel { background-color : lightgreen; }")


class Logo(QWidget):
    def __init__(self):
        super().__init__()

        # creating label
        self.label = QLabel(self)

        # loading image
        self.pixmap = QPixmap(resource_path("utidlogo.png"))

        # adding image to label
        self.label.setPixmap(self.pixmap)

        # Optional, resize label to image size
        self.label.resize(self.pixmap.width(), self.pixmap.height())


class Buttons(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.layout = QFormLayout()
        code_gas_list = LIST_GAS()

        self.e1 = QComboBox()
        for code in code_gas_list:
            self.e1.addItem(f"Paràmetres {GAS_DICT[code]}")
        self.e1.setFont(QFont("Arial", 15))
        self.e1.currentIndexChanged.connect(self.on_combobox_changed)
        self.layout.addRow(self.e1)

        self.setLayout(self.layout)
        if code_gas_list:
            code = code_gas_list[0]
            self.parent.DPNForm.show(code)
            self.parent.MCTForm.show(code)

    def on_combobox_changed(self, index):
        code = LIST_GAS()[index]
        self.parent.DPNForm.show(code)
        self.parent.MCTForm.show(code)


class RebootDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.config = parent.config
        self.setWindowTitle("Arxius FX no trobats en 1H")

        QBtn = QDialogButtonBox.Yes | QDialogButtonBox.No

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        message = QLabel("El sistema esta en parada?")
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)
        self.config.set("force_data_reboot_open", str(get_utc_actual()))
        timer = QTimer(self)
        timer.timeout.connect(self.reboot)
        timer.start(1000)

    def reboot(self):
        force_data_reboot_open = (
            parser.parse(self.config.get("force_data_reboot_open"))
            if self.config.get("force_data_reboot_open")
            else None
        )
        if force_data_reboot_open and (
            get_utc_actual() - force_data_reboot_open
        ).total_seconds() >= (60 * 30):
            self.config.set("force_data_reboot_open", False)
            reboot_system(self.config)


RUN_PATH = "Microsoft\\Windows\\CurrentVersion"
APP_NAME = "UTIDDesktop"
ORGANIZATION_NAME = "UTID"


class WorkerThread(QThread):
    def __init__(self, parent):
        QThread.__init__(self, parent)
        self.parent = parent
        self.finished = pyqtSignal()

    def start(self):
        self._timer = QTimer(self)
        self._timer.timeout.connect(self.process)
        self._timer.start(8000)

    def stop(self):
        self._timer.stop()
        self.finished.emit()

    def process(self):
        n = datetime.now(tzutc())
        n = n.astimezone(gettz("Spain/Madrid"))
        if n.second >= FX_PREPARED_SECOND:
            timestamp_datetime = n.replace(second=0, microsecond=0).timestamp()
            self.parent.last_datetime_read = timestamp_datetime
            if self.parent.last_datetime_read > self.parent.last_datetime_written:
                if set_info_DPN_MCT(self.parent.config, self.parent.last_datetime_read):
                    self.parent.last_datetime_written = timestamp_datetime
            for key, value in self.parent.config.as_dict().items():
                self.parent.qsettings.setValue(key, value)


class MainWindow(QMainWindow):
    valueChanged = pyqtSignal(object)
    _is_killed = True
    last_datetime_written = datetime(
        2022, 1, 1, tzinfo=gettz("Spain/Madrid")
    ).timestamp()
    last_datetime_read = datetime(2022, 1, 1, tzinfo=gettz("Spain/Madrid")).timestamp()

    def __init__(self, app):
        super(MainWindow, self).__init__()
        self.app = app
        self.settings = QSettingsManager()
        self.config = ConfigManager()
        self.qsettings = QSettings(ORGANIZATION_NAME, APP_NAME)
        self.initsettings = QSettings(RUN_PATH, "Run")
        self.initsettings.setValue(APP_NAME, sys.argv[0])
        self.logger = setup_logger()
        defaults = {}
        for key in self.qsettings.allKeys():
            defaults[key] = self.qsettings.value(key)
        n = datetime.now(tzutc())
        n = n.astimezone(gettz("Spain/Madrid"))
        defaults["version"] = VERSION
        defaults["initMinute"] = str(n.minute)
        defaults["initDate"] = n.replace(second=0, microsecond=0).isoformat()
        if not "Acumulat_Mitjanes_LLA_actual_year" in defaults:
            defaults["Acumulat_Mitjanes_LLA_actual_year"] = str(n.year)
        elif int(n.year) > int(defaults["Acumulat_Mitjanes_LLA_actual_year"]):
            defaults["Acumulat_Mitjanes_LLA_actual_year"] = str(n.year)
            for key in self.qsettings.allKeys():
                if key.endswith("Acumulat_Mitjanes_LLA") or key.endswith(
                    "Acumulat_Llindar"
                ):
                    defaults[key] = "0"
        if (
            not "Acumulat_Mitjanes_LLA" in defaults
            or not defaults["Acumulat_Mitjanes_LLA"]
        ):
            defaults["Acumulat_Mitjanes_LLA"] = "0"
        if not "Acumulat_Llindar" in defaults or not defaults["Acumulat_Llindar"]:
            defaults["Acumulat_Llindar"] = "0"

        global ACTUAL_LIST_GAS
        ACTUAL_LIST_GAS = LIST_GAS()

        for code in ACTUAL_LIST_GAS:
            if not code in DEFAULT_DISABLED_MCT_PERIFERIC_GAS:
                if not f"{code}_mct_enable" in defaults:
                    defaults[f"{code}_mct_enable"] = False
            if not f"{code}_dpn_enable" in defaults:
                defaults[f"{code}_dpn_enable"] = False

        self.config.set_defaults(defaults)

        ACTUAL_LIST_GAS = LIST_GAS()

        self._createMenuBar()

        self.send_thread()

        layout = QVBoxLayout()
        layout1 = QHBoxLayout()
        layout1.setContentsMargins(0, 0, 0, 0)
        layout1.setSpacing(0)

        layout1.addWidget(Logo())
        layout1.addWidget(Clock(self))
        self.WarningPanel = WarningPanel(self)
        layout1.addWidget(self.WarningPanel)

        layout.addLayout(layout1)
        self.DPNForm = DPNForm(self)
        self.MCTForm = MCTForm(self)
        layout2 = QHBoxLayout()
        layout2.addWidget(GlobalForm(self))
        layout2.addWidget(self.DPNForm)
        layout2.addWidget(self.MCTForm)

        layout.addLayout(layout2)

        widget = QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        self.label_1 = QLabel("Estat: Iniciat")
        self.valueChanged.connect(self.set_status)
        self.statusBar().addPermanentWidget(self.label_1)
        self.statusBar().addPermanentWidget(QLabel(f"{VERSION}v"))

        # Crear un botón para abrir la ventana de logs
        self.open_log_button = QPushButton("Obrir finestra de logs", self)
        self.open_log_button.clicked.connect(self.open_log_window)

        layout.addWidget(self.open_log_button)

        container = QWidget()
        container.setLayout(layout)
        self.setCentralWidget(container)

    def open_log_window(self):
        """Abre la ventana de logs."""
        self.log_window = LogWindow()
        self.log_window.show()

    def set_status(self):
        if self.is_killed:
            self.label_1.setText("Estat: Aturat")
        else:
            self.label_1.setText("Estat: Iniciat")

    def initUI(self):
        self.setWindowTitle("UTID Innovation and Development Technical Unit")
        self.setWindowIconText(APP_NAME)
        self.resize(1200, 600)
        self.center()
        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    @pyqtSlot()
    def send_thread(self):
        self.thread = WorkerThread(self)
        self.thread.start()

    def kill_g(self):
        self.thread.stop()
        self.is_killed = True

    @property
    def is_killed(self):
        return self._is_killed

    @is_killed.setter
    def is_killed(self, value):
        self._is_killed = value
        self.valueChanged.emit(value)

    @pyqtSlot()
    def show_popup(self):
        dlg = RebootDialog(self)
        if not dlg.exec():
            reboot_system(self.config)
        else:
            self.config.set("force_data_reboot_confirm", str(get_utc_actual()))

    def clear_acumulat_llindar(self):
        self.config.set("Acumulat_Llindar", str(0))
        self.qsettings.setValue("Acumulat_Llindar", str(0))
        self.config.set("text_Acumulat_Llindar", str(0))
        self.qsettings.setValue("text_Acumulat_Llindar", str(0))
        self.WarningPanel.check()

    def clear_acumulat_mitjanes_lla(self):
        self.config.set("Acumulat_Mitjanes_LLA", str(0))
        self.qsettings.setValue("Acumulat_Mitjanes_LLA", str(0))
        self.config.set("text_Acumulat_Mitjanes_LLA", str(0))
        self.qsettings.setValue("text_Acumulat_Mitjanes_LLA", str(0))
        self.WarningPanel.check()

    def clear_acumulat_hores_substitucio(self):
        self.config.set("Acumulat_Hores_Substitucio", str(0))
        self.qsettings.setValue("Acumulat_Hores_Substitucio", str(0))
        self.WarningPanel.check()

    def clear_acumulat_indisponibilitat(self, code):
        self.config.set(f"{code}_Acumulat_Indisponibilitat", str(0))
        self.qsettings.setValue(f"{code}_Acumulat_Indisponibilitat", str(0))
        self.MCTForm.update_indisponibilitat(code)

    def toogle_gas_enable(self, code, type):
        old_value = self.config.get(f"{code}_{type}_enable")
        if isinstance(old_value, str):
            value = not old_value == "true"
        else:
            value = not old_value
        self.config.set(f"{code}_{type}_enable", value)
        self.qsettings.setValue(f"{code}_{type}_enable", value)

    def _createMenuBar(self):
        menuBar = self.menuBar()
        menu = menuBar.addMenu("Sistema")

        start = menu.addAction("Iniciar")
        start.triggered.connect(self.send_thread)

        stop = menu.addAction("Aturar")
        stop.triggered.connect(self.kill_g)

        quit = menu.addAction("Tancar")
        quit.triggered.connect(self.app.quit)

        menu.addAction(start)
        menu.addAction(stop)
        menu.addAction(quit)

        menu2 = menuBar.addMenu("Alertes")

        reset = menu2.addMenu("Reiniciar")

        anomal = reset.addAction("Acumulat_Llindar")
        anomal.triggered.connect(self.clear_acumulat_llindar)

        acumulat24 = reset.addAction("Acumulat_Mitjanes_LLA")
        acumulat24.triggered.connect(self.clear_acumulat_mitjanes_lla)

        oxigen = reset.addAction("Acumulat_Hores_Substitucio")
        oxigen.triggered.connect(self.clear_acumulat_hores_substitucio)

        indisponibilitat = reset.addMenu("Acumulat_Indisponibilitat")

        for code in LIST_GAS():
            gas = indisponibilitat.addAction(GAS_DICT[code])
            gas.triggered.connect(partial(self.clear_acumulat_indisponibilitat, code))

        menu3 = menuBar.addMenu("Configuracions")
        dpn = menu3.addMenu("DPN")
        mct = menu3.addMenu("MCT")

        for code in LIST_GAS():
            gas = dpn.addAction(GAS_DICT[code])
            type = "dpn"
            gas.triggered.connect(partial(self.toogle_gas_enable, code, type))
            gas.setCheckable(True)
            old_value = self.config.get(f"{code}_{type}_enable")
            if isinstance(old_value, str) and old_value == "true":
                gas.setChecked(True)
            elif isinstance(old_value, str) and old_value == "false":
                gas.setChecked(False)
            elif isinstance(old_value, bool) and old_value:
                gas.setChecked(True)
            elif isinstance(old_value, bool) and not old_value:
                gas.setChecked(False)

            if not code in DEFAULT_DISABLED_MCT_PERIFERIC_GAS:
                type = "mct"
                gas = mct.addAction(GAS_DICT[code])
                gas.triggered.connect(partial(self.toogle_gas_enable, code, type))
                gas.setCheckable(True)
                if isinstance(old_value, str) and old_value == "true":
                    gas.setChecked(True)
                elif isinstance(old_value, str) and old_value == "false":
                    gas.setChecked(False)
                elif isinstance(old_value, bool) and old_value:
                    gas.setChecked(True)
                elif isinstance(old_value, bool) and not old_value:
                    gas.setChecked(False)

    def update_log(self):
        """Actualiza el QTextEdit con los últimos mensajes del log."""
        try:
            with open(LOG_FILEPATH, "r") as log_file:
                log_content = log_file.read()
            self.log_display.setPlainText(log_content)
        except FileNotFoundError:
            self.log_display.setPlainText("No s'ha pogut generar el fitxer de log.")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setOrganizationName(ORGANIZATION_NAME)
    app.setApplicationName(APP_NAME)
    app.setApplicationVersion(VERSION)
    app.setQuitOnLastWindowClosed(False)
    window = MainWindow(app)

    # Adding an icon
    icon = QIcon(resource_path("icon.png"))

    # Adding item on the menu bar
    tray = QSystemTrayIcon()
    tray.setIcon(icon)
    tray.setVisible(True)

    # Creating the options
    menu = QMenu()
    option0 = QAction("Obrir")
    option0.triggered.connect(window.show)
    option1 = QAction("Iniciar")
    option1.triggered.connect(window.send_thread)
    option2 = QAction("Aturar")
    option2.triggered.connect(window.kill_g)
    menu.addAction(option0)
    menu.addAction(option1)
    menu.addAction(option2)

    # To quit the app
    quit = QAction("Tancar")
    quit.triggered.connect(app.quit)
    menu.addAction(quit)

    # Adding options to the System Tray
    tray.setContextMenu(menu)
    window.initUI()
    app.setWindowIcon(QIcon(resource_path("icon.png")))

    sys.exit(app.exec_())
