from datetime import datetime
import csv, re, os
from os.path import exists
from datetime import timedelta, datetime
from dateutil.tz import tzutc, gettz
from dateutil import parser

from uart import SerialWrapper

ENV = "production"
DEV_GAS_LIST = []
# ENV = "development"
# DEV_GAS_LIST = ["252", "261", "220"]


GAS_DICT = {
    "109": "Pressió",
    "110": "Temperatura",
    "112": "Partícules",
    "130": "Humitat H2O(v)",
    "142": "Hg",
    "212": "SO2",
    "220": "NOx",
    "221": "NO",
    "223": "NH3",
    "224": "NH3 net",
    "234": "HCI",
    "241": "NO2",
    "252": "O2",
    "261": "CO",
    "262": "CO2",
    "270": "COT",
    "280": "HF",
    "349": "Caudal a depurar*2",
    "350": "Caudal de gasos emesos*3",
    "352": "Potencia",
    "353": "Tipus de combustible",
    "354": "Potencia de turbina",
    "362": "Temperatura de la càmara 1",
}


def reboot_system(config):
    config.set("force_nofile_date_reboot", False)
    config.set("force_data_reboot", False)
    config.set("force_data_reboot_confirm", False)
    # serial = SerialWrapper()
    # serial.sendData(data)
    if ENV == "production":
        os.system("shutdown /r /t 1")
    else:
        print("shutdown")


"""
Contenido del archivo de origen
fichero FX
1 código identificador al inicio (formado por 3 dígitos)
60 valores, es decir un valor cada 1 minuto
Para seleccionar los valores es preferible contar como separador las unidades
(carácter alfanumérico) e incluirlo en el anterior dato númerico, finalmente
realizar un strip() para eliminar espacios.
"""


def split_on_first_alpha(i):
    splitted_numbers = re.split("([a-zA-Z])", i)

    numbers = []
    while len(splitted_numbers) > 1:
        number = splitted_numbers.pop(0).strip()
        unit = splitted_numbers.pop(0)
        numbers.append(f"{number}{unit}")
    return numbers


def decode_origin_file(line):
    code = line[:3]
    data = split_on_first_alpha(line[3:])
    return (code, data)


"""
Ejemplo fileinfo:
{
    '252': [
        '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V', '4.3V'
    ],
    '261': [
        '22.4R', '22.5V', '22.3V', '22.6N', '22.6V', '22.9V', '22.6A', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V', '22.6V'
    ]
}

"""


def get_data_from_path(path, config):
    file_exists = exists(path)
    fileinfo = {}
    no_data_key = "0000N"
    no_data_code = []
    count_code = 0
    if file_exists:
        with open(path) as file:
            for line in file:
                count_code += 1
                code, data = decode_origin_file(line)
                fileinfo[code] = data
                if data == [no_data_key] * 60:
                    no_data_code.append(code)
        if len(no_data_code) == count_code:
            force_data_reboot = (
                parser.parse(config.get("force_data_reboot"))
                if config.get("force_data_reboot")
                else None
            )

            if not force_data_reboot:
                config.set("force_data_reboot", str(get_utc_actual()))

        else:
            config.set("force_nofile_date_reboot", False)
            config.set("force_data_reboot", False)
            config.set("force_data_reboot_confirm", False)

    else:
        force_nofile_date_reboot = (
            parser.parse(config.get("force_nofile_date_reboot"))
            if config.get("force_nofile_date_reboot")
            else None
        )

        if not force_nofile_date_reboot:
            config.set("force_nofile_date_reboot", str(get_utc_actual()))
        elif (get_utc_actual() - force_nofile_date_reboot).total_seconds() >= 3600:
            reboot_system(config)
        return {}

    return fileinfo


def get_list_gas(path, config):
    info = get_data_from_path(path, config)
    return list(info.keys())


"""
Input:
Los datos de origen se encuentran en el directorio: C:\Rdiari\FX\@Año\@Mes.
Ejemplo: C:\Rdiari\FX\22\01
El nombre del fichero esta compuesto por las letras FX
La fecha actual expresada como día/mes/año
En la extensión se añade .MX, en donde la X hace referencia a la hora actual
Ejemplo: M00 a M23
Ejemplo final nombre archivo: FX110122.M13
Ruta definitiva ejemplo: C:\Rdiari\FX\22\01\FX110122.M13
"""


def fx_filepath_date(currentYear, currentMonth, currentDay, currentHour):
    if ENV == "production":
        origin_path = f"C:\Rdiari\FX\{currentYear}\{currentMonth}"
    else:
        origin_path = "FX"
    filename = f"FX{currentDay}{currentMonth}{currentYear}.M{currentHour}"
    return f"{origin_path}\{filename}"


def fx_filepath_time(currentHour):
    if not isinstance(currentHour, str):
        currentHour = str(currentHour).zfill(2)
    n = datetime.now(tzutc())
    n = n.astimezone(gettz("Spain/Madrid"))
    currentYear = str(n.year)[2:]
    currentMonth = str(n.month).zfill(2)
    currentDay = str(n.day).zfill(2)
    return fx_filepath_date(currentYear, currentMonth, currentDay, currentHour)


def fx_filepath():
    n = datetime.now(tzutc())
    n = n.astimezone(gettz("Spain/Madrid"))
    currentHour = str(n.hour).zfill(2)
    return fx_filepath_time(currentHour)


def LIST_GAS():
    return list(GAS_DICT.keys())


"""
Output:
Se almacenará en dos ficheros CSV denominados DPN y MCT
Directorio C:\@año\@mes
Ejemplo: C:\2022\01
Definición nombre archivo
1. Indica si contiene datos DPN (de primer nivel) o MCT (medianas de corto plazo).
2. Campo identificador del establecimiento, formado por 5 números, se incluyen los ceros a la izquierda.
3. Guion bajo.
4. Fecha en formato número del año completo, mes y día sin separación.
5. Extensión del archivo CSV.

Ejemplo DPN: DPN00001_20220111.csv
Ejemplo MCT: MCT00001_20220111.csv
"""


def DPN_filepath(codiemis):
    n = datetime.now(tzutc())
    n = n.astimezone(gettz("Spain/Madrid"))
    currentYear = str(n.year)
    currentMonth = str(n.month).zfill(2)
    currentDay = str(n.day).zfill(2)
    if ENV == "production":
        folder = f"C:\DadesXEAC\{currentYear}\{currentMonth}"
    else:
        folder = "DadesXEAC"
    os.makedirs(folder, exist_ok=True)
    filepath = f"{folder}\DPN{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
    return filepath


def MCT_filepath(codiemis):
    n = datetime.now(tzutc())
    n = n.astimezone(gettz("Spain/Madrid"))
    currentYear = str(n.year)
    currentMonth = str(n.month).zfill(2)
    currentDay = str(n.day).zfill(2)
    if ENV == "production":
        folder = f"C:\DadesXEAC\{currentYear}\{currentMonth}"
    else:
        folder = "DadesXEAC"
    os.makedirs(folder, exist_ok=True)
    filepath = f"{folder}\MCT{codiemis}_{currentYear}{currentMonth}{currentDay}.csv"
    return filepath


"""
Example info:
{
  '252_NSerie': '',
  '252_FC(c)x2': '1',
  '252_FC(b)x': '',
  '252_FC(a)': '',
  '252_Ival': '',
  '252_IDUnitat': '',
  '261_NSerie': '1',
  '261_FC(c)x2': '',
  '261_FC(b)x': '',
  '261_FC(a)': '',
  '261_Ival': '',
  '261_IDUnitat': '',
  '252_NumMin': '60',
  '252_F_VLE': '',
  '252_V_IMP': '',
  '252_F_IMP': '',
  '252_V_INV': '',
  '252_F_INV': '',
  '252_V_PRT': '',
  '252_F_PRT': '',
  '252_V_GEH': '',
  '252_F_GEH': '',
  '261_NumMin': '10',
  '261_F_VLE': '',
  '261_V_IMP': '',
  '261_F_IMP': '',
  '261_V_INV': '',
  '261_F_INV': '',
  '261_V_PRT': '',
  '261_F_PRT': '',
  '261_V_GEH': '',
  '261_F_GEH': '',
  'CodiEmis': '123',
  'IDFocus': '2',
  'IDLegis': '1',
  'IDSubmode': '3'
}
"""


def parseMinutesToText(minutes):
    hours = minutes // 60
    minutes = minutes % 60
    return "{}h {}m".format(hours, minutes)


def parseValueNULL(value):
    if not value:
        return "NULL"
    return value


def parseValueNN(value):
    if not value:
        return "NN"
    return value


def parseExponentFloat(value):
    if not value or not float(value):
        value = 0
    return "{:.6e}".format(float(value)).upper()


def set_info_DPN(info, code, marca_temps, config):
    headers = [
        "CodiEmis",
        "IDFocus",
        "MarcaTemps",
        "IDParametre",
        "NSerie",
        "FC(c)x2",
        "FC(b)x",
        "FC(a)",
        "Ival",
        "IDLegis",
        "IDSubmode",
        "Valor",
        "CV",
        "IDUnitat",
    ]
    codiemis = info["CodiEmis"].zfill(5)
    idfocus = info["IDFocus"]
    marcatemps = get_plain_datetime_actual(marca_temps)
    nserie = info[f"{code}_NSerie"]
    fccx2 = parseExponentFloat(info[f"{code}_FC(c)x2"])
    fcbx = parseExponentFloat(info[f"{code}_FC(b)x"])
    fca = parseExponentFloat(info[f"{code}_FC(a)"])
    ival = parseExponentFloat(info[f"{code}_Ival"])
    idlegis = info["IDLegis"]
    idsubmode = info["IDSubmode"]
    pathdata = fx_filepath()
    fx_data = get_data_from_path(pathdata, config)
    if code in fx_data:
        pos = marca_temps.minute
        if len(fx_data[code]) >= pos:
            valor = fx_data[code][pos][:-1]
            cv = fx_data[code][pos][-1]
            if code == "252" and cv == "S":
                count = config.get(f"Acumulat_Hores_Substitucio")
                if not count:
                    count = 0
                else:
                    count = float(count)
                count += 1 / 60
                config.set(f"Acumulat_Hores_Substitucio", str(count))
        else:
            valor = 0
            cv = "N"
    else:
        valor = 0
        cv = "N"
    idunitat = info[f"{code}_IDUnitat"]
    filepath = DPN_filepath(codiemis)

    data = [
        parseValueNULL(codiemis),
        parseValueNULL(idfocus),
        parseValueNULL(marcatemps),
        parseValueNULL(code),
        parseValueNULL(nserie),
        parseExponentFloat(fccx2),
        parseExponentFloat(fcbx),
        parseExponentFloat(fca),
        parseExponentFloat(ival),
        parseValueNULL(idlegis),
        parseValueNULL(idsubmode),
        parseExponentFloat(valor),
        parseValueNULL(cv),
        parseValueNULL(idunitat),
    ]
    write_file(filepath, headers, data)
    return True


def is_report_values(values):
    count = 0
    for value in values:
        if value.endswith("C") or value.endswith("N") or value.endswith("V"):
            count += 1
    return (count / len(values)) >= (2 / 3)


def calc_vinc(values, c6, c7, c8, cabal):
    success_values = []
    for value in values:
        if value.endswith("V") or value.endswith("A"):
            success_values.append(float(value[:-1]))
    if success_values:
        x = sum(success_values) / len(success_values)
        calibration = calc_calibration(x, c6, c7, c8)
        result = parseExponentFloat(calibration * cabal)
    else:
        result = "NULL"
    return result


def calc_calibration(x, c6, c7, c8):
    return c6 * x * x + c7 * x + c8


def calc_finv(values):
    count = 0
    for value in values:
        if value.endswith("A") or value.endswith("V"):
            count += 1
    if (count / len(values)) >= (2 / 3):
        result = "RS"
    else:
        result = "NN"
    return result


def set_info_MCT(config, code, marca_temps, periode, llindar_no_afectat):
    info = config.as_dict()
    headers = [
        "CodiEmis",
        "IDFocus",
        "MarcaTemps",
        "NumMinuts",
        "IDParametre",
        "IDLegis",
        "IDSubmode",
        "V_VLE",
        "F_VLE",
        "V_IMP",
        "F_IMP",
        "V_INV",
        "F_INV",
        "V_PRT",
        "F_PRT",
        "V_GEH",
        "F_GEH",
    ]

    codiemis = info["CodiEmis"].zfill(5)
    idfocus = info["IDFocus"]
    marcatemps = get_plain_datetime_actual(marca_temps)
    numminuts = info[f"{code}_NumMin"]
    idlegis = info["IDLegis"]
    idsubmode = info["IDSubmode"]
    cabal = float(info["Cabal"]) if info["Cabal"] else 0
    vimp = "NULL"
    fimp = info[f"{code}_F_IMP"]
    vgeh = parseExponentFloat(info[f"{code}_V_GEH"])
    fgeh = info[f"{code}_F_GEH"]
    llindaranomal = (
        float(info[f"{code}_Llindar_Anomal"]) if info[f"{code}_Llindar_Anomal"] else 0
    )
    nmitjaneslla = float(info["N_Mitjanes_LLA"]) if info["N_Mitjanes_LLA"] else 0
    fx_data = get_data_from_path(fx_filepath(), config)
    if not fx_data:
        fx_data[code] = ["0000N"] * 60
    fx_pasthour_data = get_data_from_path(
        fx_filepath_time((marca_temps - timedelta(hours=1)).hour), config
    )
    if not fx_pasthour_data:
        fx_pasthour_data[code] = ["0000N"] * 60
    actualminute = marca_temps.minute
    if actualminute == 0 and code in fx_pasthour_data:
        values = fx_pasthour_data[code][(periode * -1) :]
    elif actualminute - periode >= 0 and code in fx_data:
        values = fx_data[code][actualminute - periode : actualminute]
    else:
        actualvalues = fx_data[code][0:actualminute]
        if code in fx_pasthour_data:
            pastvalues = fx_pasthour_data[code][(actualminute - periode) :]
            values = actualvalues + pastvalues
        else:
            values = actualvalues

    success_values = []
    for value in values:
        if value.endswith("V"):
            success_values.append(float(value[:-1]))

    report = is_report_values(values)

    fvle = None
    vvle = None
    c6 = float(info[f"{code}_FC(c)x2"]) if info[f"{code}_FC(c)x2"] else 0
    c7 = float(info[f"{code}_FC(b)x"]) if info[f"{code}_FC(b)x"] else 0
    c8 = float(info[f"{code}_FC(a)"]) if info[f"{code}_FC(a)"] else 0

    if periode == 10:
        vinv = "NULL"
        finv = "NN"
        vprt = "NULL"
        fprt = "NN"
    else:
        vinv = calc_vinc(values, c6, c7, c8, cabal)
        finv = calc_finv(values)
        vprt = vinv
        fprt = finv
    acumulat_llindar = int(info["Acumulat_Llindar"])
    acumulat_mitjanes = int(info["Acumulat_Mitjanes_LLA"])
    ic = float(info[f"{code}_IC"]) if info[f"{code}_IC"] else 0
    if success_values:
        median = sum(success_values) / len(success_values)
        x = float(median)
        calibration = calc_calibration(x, c6, c7, c8)
        if calibration < float(llindaranomal):
            vvle = calibration - calibration * ic
        else:
            vvle = calibration - float(llindaranomal) * ic
        if vvle < 0:
            vvle = 0

        if report:
            if (len(success_values) / len(values)) >= (2 / 3):
                config.set(f"{code}_Acumulat_Indisponibilitat", str(0))
                config.set(f"{code}_Indisponibilitat", str(0))
                fvle = "RV"
                if median > float(llindaranomal):
                    if acumulat_llindar > float(nmitjaneslla):
                        if llindar_no_afectat:
                            llindar_no_afectat = False
                            acumulat_mitjanes += int(numminuts)
                            config.set("Acumulat_Mitjanes_LLA", str(acumulat_mitjanes))
                            config.set(
                                "text_Acumulat_Mitjanes_LLA",
                                parseMinutesToText(acumulat_mitjanes),
                            )
                        fvle = "VA"
                    else:
                        if llindar_no_afectat:
                            llindar_no_afectat = False
                            acumulat_llindar += int(numminuts)
                            config.set("Acumulat_Llindar", str(acumulat_llindar))
                            config.set(
                                "text_Acumulat_Llindar",
                                parseMinutesToText(acumulat_llindar),
                            )
                        fvle = "VD"
                else:
                    fvle = "VD"
            else:
                indisponibilitat = (
                    float(info[f"{code}_Indisponibilitat"])
                    if f"{code}_Indisponibilitat" in info
                    and info[f"{code}_Indisponibilitat"]
                    else 0
                )
                acumulat_indisponibilitat = (
                    int(info[f"{code}_Acumulat_Indisponibilitat"])
                    if f"{code}_Acumulat_Indisponibilitat" in info
                    and info[f"{code}_Acumulat_Indisponibilitat"]
                    else 0
                )
                if indisponibilitat > (6 * 60):
                    config.set(f"{code}_Indisponibilitat", str(0))
                    acumulat_indisponibilitat += 1
                    config.set(
                        f"{code}_Acumulat_Indisponibilitat",
                        str(acumulat_indisponibilitat),
                    )
                else:
                    indisponibilitat += periode
                    config.set(f"{code}_Indisponibilitat", str(indisponibilitat))
                fvle = "RN"
        else:
            fvle = "NN"
    else:
        median = 0

    filepath = MCT_filepath(codiemis)
    data = [
        parseValueNULL(codiemis),
        parseValueNULL(idfocus),
        parseValueNULL(marcatemps),
        parseValueNULL(numminuts),
        parseValueNULL(code),
        parseValueNULL(idlegis),
        parseValueNULL(idsubmode),
        parseExponentFloat(vvle),
        parseValueNN(fvle),
        parseValueNULL(vimp),
        parseValueNN(fimp),
        parseValueNULL(vinv),
        parseValueNN(finv),
        parseValueNULL(vprt),
        parseValueNN(fprt),
        parseValueNULL(vgeh),
        parseValueNN(fgeh),
    ]
    write_file(filepath, headers, data)
    return llindar_no_afectat


def is_last_time_done(filepath, actual_utc):
    data = read_file_last_line(filepath)
    if data:
        marca_temps = data.split(";")[2]
        return get_datetime_from_plain(marca_temps) == actual_utc
    return False


def read_file_last_line(filepath):
    file_exists = exists(filepath)
    if file_exists:
        csvfile = open(filepath)
        lastline = csvfile.readlines()[-1]
        csvfile.close()
        return lastline
    return None


def write_file(filepath, headers, data):
    file_exists = exists(filepath)
    csvfile = open(filepath, "a", newline="")
    csvwriter = csv.writer(csvfile, delimiter=";")
    if not file_exists:
        csvwriter.writerow(headers)
    csvwriter.writerow(data)
    del csvwriter
    csvfile.close()


def get_plain_datetime_actual(get_utc_actual):
    return str(get_utc_actual).replace("+", " +")


def get_utc_actual():
    utc = datetime.now(tzutc())
    local = utc.astimezone(gettz("Spain/Madrid"))
    return local - timedelta(seconds=local.second, microseconds=local.microsecond)


def parse_timestmap(timestamp):
    utc = datetime.fromtimestamp(timestamp)
    local = utc.astimezone(gettz("Spain/Madrid"))
    return local - timedelta(seconds=local.second, microseconds=local.microsecond)


def get_datetime_from_plain(plain_date):
    return datetime.strptime(plain_date, "%Y-%m-%d %H:%M:%S %z")


"""
This methods is executed every minute called by the interface
"""


def set_info_DPN_MCT(config, actualdate):
    info = config.as_dict()
    if ENV == "production":
        enabled_mct_periferic_gas = [
            k.split("_")[0]
            for k, v in info.items()
            if ((isinstance(v, str) and v == "true") or (isinstance(v, bool) and v))
            and k.endswith("mct_enable")
        ]
        enabled_dpn_periferic_gas = [
            k.split("_")[0]
            for k, v in info.items()
            if ((isinstance(v, str) and v == "true") or (isinstance(v, bool) and v))
            and k.endswith("dpn_enable")
        ]
    elif len(DEV_GAS_LIST):
        enabled_mct_periferic_gas = DEV_GAS_LIST
        enabled_dpn_periferic_gas = DEV_GAS_LIST
    else:
        enabled_mct_periferic_gas = LIST_GAS()
        enabled_dpn_periferic_gas = LIST_GAS()

    result = False
    actual_marca_temps = parse_timestmap(actualdate)
    llindar_no_afectat = True

    for code in list(set(LIST_GAS()).intersection(enabled_mct_periferic_gas)):
        if (
            actualdate != info["initDate"]
            and actual_marca_temps.minute % int(info[f"{code}_NumMin"]) == 0
        ):
            llindar_no_afectat = set_info_MCT(
                config,
                code,
                actual_marca_temps,
                int(info[f"{code}_NumMin"]),
                llindar_no_afectat,
            )
    if not is_last_time_done(
        DPN_filepath(info["CodiEmis"].zfill(5)), actual_marca_temps
    ):
        for code in LIST_GAS():
            if code in enabled_dpn_periferic_gas:
                result = set_info_DPN(info, code, actual_marca_temps, config)

    return result
